const config = {
    rabbit_mq: process.env.STC_BROKER_URL || "amqp://rabbitmq",
    photos_dir: process.env.STC_STORAGE_PATH || "/opt/stc/storage"
};

module.exports = config;
