const amqp = require('amqplib');
const resemble = require('node-resemble-js');
const fs = require('fs');

var payload = {
    place: 1,
    source: 1,
    compare_with: 2
};


amqp.connect('amqp://rabbitmq').then(function (conn) {
    return conn.createChannel().then(function (ch) {
        var q = 'resemble';
        var msg = JSON.stringify(payload);

        var ok = ch.assertQueue(q, {durable: false});

        return ok.then(function (_qok) {
            // NB: `sentToQueue` and `publish` both return a boolean
            // indicating whether it's OK to send again straight away, or
            // (when `false`) that you should wait for the event `'drain'`
            // to fire before writing again. We're just doing the one write,
            // so we'll ignore it.
            ch.sendToQueue(q, Buffer.from(msg));
            console.log(" [x] Sent '%s'", msg);
            return ch.close();
        });
    }).finally(function () {
        conn.close();
    });
}).catch(console.warn);