const path = require('path');
const fs = require('fs');
const amqp = require('amqplib');
const resemble = require('node-resemble-js');
const config = require('./config');


amqp.connect(config.rabbit_mq).then((conn) =>
    conn.createChannel().then((ch) => {
        ch.assertQueue('resemble', {durable: false})
            .then((_qok) =>
                ch.consume('resemble', (msg) =>
                    process(JSON.parse(msg.content.toString())
                    ).then((result) => {
                        console.log(result);
                        ch.sendToQueue(
                            msg.properties.replyTo,
                            new Buffer(JSON.stringify(result.misMatchPercentage)),
                            {correlationId: msg.properties.correlationId}
                        );
                    }).catch((err) => {
                        console.log(err)
                        ch.sendToQueue(
                            msg.properties.replyTo,
                            new Buffer(JSON.stringify(err)),
                            {correlationId: msg.properties.correlationId}
                        );
                    }), {noAck: false})
            );
    })
).catch(console.warn);

let process = (payload) =>
    new Promise((resolve, reject) => {
        let place_path = path.join(config.photos_dir, payload.place);
        let source_img = path.join(place_path, payload.source);
        let compare_img = path.join(place_path, payload.compare_with);
        if (!fs.existsSync(source_img) || !fs.existsSync(compare_img)) {
            reject('invalid paths');
        }
        resemble(source_img)
            .compareTo(compare_img)
            .ignoreColors()
            .ignoreAntialiasing()
            .onComplete((data) => {
                resolve(data)
            })
    });
