FROM node

ADD . /resemble-worker
WORKDIR /resemble-worker

RUN npm i

ENTRYPOINT npm start
